package ru.cinarra.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.cinarra.entity.PersonEntity;

import java.util.List;

public interface PersonRepository extends JpaRepository<PersonEntity, Integer> {
    @Query("SELECT e FROM PersonEntity e WHERE LOWER(e.nameFamilyPatronymic) LIKE LOWER(:name)")
    List<PersonEntity> findByName(@Param("name") String name);
}
