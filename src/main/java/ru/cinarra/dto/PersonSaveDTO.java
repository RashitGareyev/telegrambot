package ru.cinarra.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PersonSaveDTO {
    @NotNull
    private String nameFamilyPatronymic;
    @NotNull
    private String rulePeriod;
}
