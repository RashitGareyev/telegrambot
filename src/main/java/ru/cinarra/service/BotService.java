package ru.cinarra.service;

import org.springframework.stereotype.Service;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.AnswerCallbackQuery;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.cinarra.entity.PersonEntity;
import ru.cinarra.repository.PersonRepository;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Service
public class BotService extends TelegramLongPollingBot {
    private final PersonRepository repository;

    public BotService(PersonRepository repository) {
        this.repository = repository;
    }

    @Override
    public String getBotToken() {
        return "697635550:AAEjmWeIlGnwND76lcXN2658012UNAVqDP8";
    }

    @Override
    public String getBotUsername() {
        return "JavaJarvisBot";
    }

    @Override
    public void onUpdateReceived(Update update) {
        messageHandler(update);
        callbackQueryHandler(update);
    }


    private void messageHandler(@NotNull Update update) {
        Message message = update.getMessage();
        if (message != null && message.hasText()) {
            switch (message.getText()) {
                case "/start":
                    sendMsg(message, "Введите ФИО любого правителя России за всю историю. Если Вы не найдете интересующего вас правителя, то Вы можете добавить о нем информацию", false);
                    break;
                case "Добавить правителя":
                    sendMsg(message, "Напишите правильно ФИО в формате: " + "\"Иванов Иван Иванович\"" + " и нажмите на ввод", false);
                    break;
                default:
                    if (getListOfPersons(message.getText()).size() > 0) {
                        for (PersonEntity person : getListOfPersons(message.getText())) {
                            sendMsg(message, getPersonInfo(person), true);
                        }
                    } else {
                        sendMsg(message, "Такого правителя нет в базе данных", false);
                    }
            }
        }
    }


    private void callbackQueryHandler(@NotNull Update update) {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        if (callbackQuery != null) {
            switch (callbackQuery.getData()) {
                case "Редактировать ФИО":
                    answerCallbackQuery(callbackQuery.getId(), "Напишите правильно ФИО в формате: " + "\"Иванов Иван Иванович\"" + " и нажмите на ввод");
                    break;
                case "Редактировать период правления":
                    answerCallbackQuery(callbackQuery.getId(), "Напишите правильно период правления в формате: " + "\"ХХХХ-ХХХХ гг.\"" + " и нажмите на ввод");
                    break;
            }
        }
    }


    private void sendMsg(@NotNull Message message, String text, boolean flagForPersonEdit) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        if (flagForPersonEdit) {
            setInline(sendMessage);
        } else {
            setButtons(sendMessage);
        }


        try {
            execute(sendMessage);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    private void answerCallbackQuery(String callbackId, String message) {
        AnswerCallbackQuery answerCallbackQuery = new AnswerCallbackQuery();
        answerCallbackQuery.setCallbackQueryId(callbackId);
        answerCallbackQuery.setText(message);
        answerCallbackQuery.setShowAlert(true);
        try {
            execute(answerCallbackQuery);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    private List<PersonEntity> getListOfPersons(String name) {
        return repository.findByName("%" + name + "%");
    }

    private String getPersonInfo(@NotNull PersonEntity person) {
        String result = person.getNameFamilyPatronymic() + "\n" + "Период правления: " + person.getRulePeriod();
        return result;
    }


    private void setInline(@NotNull SendMessage sendMessage) {
        List<List<InlineKeyboardButton>> buttons = new ArrayList<>();
        List<InlineKeyboardButton> buttons1 = new ArrayList<>();
        buttons1.add(new InlineKeyboardButton().setText("Редактировать ФИО").setCallbackData("Редактировать ФИО"));
        List<InlineKeyboardButton> buttons2 = new ArrayList<>();
        buttons2.add(new InlineKeyboardButton().setText("Редактировать период правления").setCallbackData("Редактировать период правления"));
        buttons.add(buttons1);
        buttons.add(buttons2);

        InlineKeyboardMarkup markupKeyboard = new InlineKeyboardMarkup();
        sendMessage.setReplyMarkup(markupKeyboard);
        markupKeyboard.setKeyboard(buttons);
    }

    private void setButtons(@NotNull SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List<KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();

        keyboardFirstRow.add(new KeyboardButton("Добавить правителя"));
        keyboardRowList.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboardRowList);
    }


}
