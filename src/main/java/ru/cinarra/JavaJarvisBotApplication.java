package ru.cinarra;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.cinarra.entity.PersonEntity;
import ru.cinarra.repository.PersonRepository;
import ru.cinarra.service.BotService;

@SpringBootApplication
public class JavaJarvisBotApplication {

    public static void main(String[] args) {
        ApiContextInitializer.init();
        TelegramBotsApi botsApi=new TelegramBotsApi();


        ConfigurableApplicationContext context= SpringApplication.run(JavaJarvisBotApplication.class, args);



        PersonRepository repository=context
                .getBean(PersonRepository.class);

        try {
            botsApi.registerBot(new BotService(repository));
        } catch (TelegramApiRequestException e) {
            e.printStackTrace();
        }

        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Путин Владимир Владимирович",
                        "2000-2008 гг., с 2012 по настоящее время"
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Медведев Дмитрий Анатольевич",
                        "2008-2012 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Ельцин Борис Николаевич",
                        "1991-1999 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Горбачев Михаил Сергеевич",
                        "1985-1991 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        " Черненко Константин Устинович",
                        "1984-1985 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        " Андропов Юрий Владимирович",
                        "1982-1984 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Брежнев Леонид Ильич",
                        "1964-1982 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Хрущев Никита Сергеевич",
                        "1953-1964 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Маленков Георгий Максимилианович",
                        "1953-1955 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Сталин Иосив Виссарионович",
                        "1924-1953 гг."
                ));
        context
                .getBean(PersonRepository.class)
                .save(new PersonEntity(
                        0,
                        "Ленин Владимир Ильич",
                        "1917-1924 гг."
                ));
    }
}
